const express = require('express');
const router = express.Router();

 
const video_controller = require('../controllers/videos.controller');


router.post('/add', video_controller.add_video);

router.get('/:id', video_controller.get_video);

router.put('/:id/update', video_controller.update_video);

router.delete('/:id/delete', video_controller.delete_video);


module.exports = router;



