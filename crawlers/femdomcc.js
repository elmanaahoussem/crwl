const Crawler = require('./Crawler');
const cheerio = require('cheerio');
const request = require('request-promise');


class Femdomcc extends Crawler {

    constructor () {
        super ("Femdomcc",
            "https://femdomcc.net/",
            "https://femdomcc.net/page/",
            "https://femdomcc.net/dirty-talk-jerkkkhxx/67059-goddess-jessica-jerk-off-jurisdiction.html")
        this.currentNavPage = 1
    }

    nextNavPage (page = null) {
        if(page){
            return this.WebNavUrl + page //Get A Specific Page Nav Url
        }
        return this.WebNavUrl + this.currentNavPage++
    }

    async crawlNavPage ( navUrl,videosTillDate = new Date(),videosFromDate = null) {

        let SingleUrls = []
        let varDate = null
        var options = {
            uri: navUrl,
            transform:   (body) => {
                const $ = cheerio.load(body);
                SingleUrls  = $("#dle-content .gray-block").map( function () {
                    varDate =   $(this).find(".vis").children().last().text().replace("Added:","").trim().split('-').reverse().join('/')
                    if( (new Date (varDate ) <= videosTillDate) && (videosFromDate?(new Date (varDate ) >= videosFromDate):true) )
                        return $(this).find(".highslide_new").attr("href")
                })
                return SingleUrls
            }
        };
        try {
            return await request(options)
        }catch (e) {
            this.logger.logError(this.WebName+" crawlNavPage Request Failed "+e)
            return Promise.reject(this.WebName +' crawlNavPage Failed')
        }
    }

    async validateNavMainElements ($,latestSkeleton) {
        const links = $("#dle-content .gray-block").length > 0
        if (links) {
            await this.saveWebsiteSkeleton("navpage_skeleton",latestSkeleton)
            return true
        }
        this.logger.logWarning(this.WebName,"validateNavMainElements","One Of The Main Inputs Missing")
        return false
    }

    async crawlSingle (url = this.WebSingleUrl) {
        let videoObject = {
            name: null,
            description: null,
            tags : null,
            category : null,
            thumbnail : null,
            link : null,
            source : this.WebName,
            date : null ,
        }
        var options = {
            uri: url,
            transform:   (body) => {
                const $ = cheerio.load(body);
                $("#dle-content .fle script").html('')
                videoObject.name = $('.ntitle_block h1').text().trim();
                videoObject.description = $("#dle-content .fle").text().trim().substring($("#dle-content .fle").text().trim().indexOf("File Name"), -1)

                videoObject.tags =  this.arrayToJson($("#dle-content .scriptcode span").map(function() {
                    if($(this).text().trim())
                        return $(this).text().trim()
                }))
                videoObject.category = $("#dle-content .vis strong").text().trim()
                videoObject.thumbnail = $("#dle-content .fle .highslide").first().attr('href').replace("?site=femdomcc.com","")
                videoObject.link = $("#dle-content iframe").attr('src').replace('preview','file')
                videoObject.date = new Date($("#dle-content .vis").children().last().text().replace("Added:","").trim().split('-').reverse().join('/'))

                return videoObject
            }
        };
        try {
            return await request(options)
        }catch (e) {
            this.logger.logError(this.WebName+" crawlSingle Request Failed "+e)
            return Promise.reject(this.WebName +' crawlSingle Failed')
        }
    }

    async validateSingleMainElements ($,latestSkeleton) {
        const title = $('.ntitle_block h1').length  > 0
        const description = $("#dle-content .fle").length > 0
        const tags  = $("#dle-content .scriptcode span").length > 0
        const category = $("#dle-content .vis strong").length > 0
        const thumbnail = $("#dle-content .fle .highslide").length > 0
        const link = $("#dle-content iframe").length > 0
        const date = $("#dle-content .vis").length > 0

        if (!(category && description)) {
            this.logger.logWarning(this.WebName,"validateSingleMainElements","Category OR Description Missing")

        }
        if (category && description && title && tags && link && date && thumbnail) {
            await this.saveWebsiteSkeleton("single_skeleton",latestSkeleton)
            return true
        }
        if (title && tags && link && date && thumbnail) {
            return true
        }
        this.logger.logWarning(this.WebName,"validateSingleMainElements","One Of The Main Inputs Missing")
        return false
    }


}

module.exports = Femdomcc;