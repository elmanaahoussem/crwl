const Crawler = require('./Crawler');
const cheerio = require('cheerio');
const request = require('request-promise');


class Footfetishbb extends Crawler {

    constructor () {
        super ("Footfetishbb",
        "https://footfetishbb.net/",
        "https://footfetishbb.net/page/",
        "https://footfetishbb.net/solestoesss/16574-janira-wolfe-first-date-phallic-toe-faggot-training.html")
        this.currentNavPage = 1
    }

    nextNavPage (page = null) { 
        if(page){ 
            return this.WebNavUrl + page //Get A Specific Page Nav Url
        } 
        return this.WebNavUrl + this.currentNavPage++  
    } 

    async crawlNavPage ( navUrl,videosTillDate = new Date(),videosFromDate = null) {
        let SingleUrls = []
        var options = {
            uri: navUrl,
            transform:   (body) => { 
                const $ = cheerio.load(body);  
                SingleUrls  = $("#dle-content article").map( function () { 
                        if( (new Date ($( this).find( "[itemprop='datePublished'] ").attr('content') ) <= videosTillDate) && (videosFromDate?(new Date ($( this).find( "[itemprop='datePublished'] ").attr('content') ) >= videosFromDate):true) )
                         return $(this).find(".short_title a").attr("href")
                      })
                    
                return SingleUrls
            }
        };
        try {
            return await request(options)   
        }catch (e) {
             this.logger.logError(this.WebName+" crawlNavPage Request Failed "+e)
             return Promise.reject(this.WebName +' crawlNavPage Failed')
        } 
    }
 
    async crawlSingle (url = this.WebSingleUrl) {
        let videoObject = {
            name: null,
            description: null,
            tags : null,
            category : null,
            thumbnail : null,
            link : null,
            source : this.WebName,
            date : null ,
        }
        var options = {
            uri: url,
            transform:   (body) => { 
                const $ = cheerio.load(body); 

                videoObject.name = $('#news-title h1').text().trim();
                videoObject.description = $("div[itemprop='articleBody']").text().trim().substring($("div[itemprop='articleBody']").text().trim().indexOf("MP4"), -1)

                videoObject.tags =  this.arrayToJson($("#dle-content .tags span").map(function() {
                                        if($(this).text().trim())
                                        return $(this).text().trim()
                                    }))
                videoObject.category = $(".link-category.post_info_item").text()
                videoObject.thumbnail = $("article.fullstory.cf meta[itemprop='image']").attr('content')
                videoObject.link = $("a[title='Download arhiv']").attr("href") 
                videoObject.date = new Date($(".post_info_item[title='Post Date'] [itemprop='datePublished']").attr('content'))                       
               
                return videoObject
            }
        };
        try {
            return await request(options)   
        }catch (e) {
             this.logger.logError(this.WebName+" crawlSingle Request Failed "+e)
             return Promise.reject(this.WebName +' crawlSingle Failed')
        }
    }

    async validateSingleMainElements ($,latestSkeleton) {
      const title = $('#news-title h1').length  > 0
      const description = $("div[itemprop='articleBody']").length > 0
      const tags  = $("#dle-content .tags span").length > 0
      const category = $(".link-category.post_info_item").length > 0
      const thumbnail = $("article.fullstory.cf meta[itemprop='image']").length > 0
      const link = $("a[title='Download arhiv']").length > 0
      const date = $(".post_info_item[title='Post Date'] [itemprop='datePublished']").length > 0 
 
      if (!(category && description)) {
        this.logger.logWarning(this.WebName,"validateSingleMainElements","Category OR Description Missing")
      
      }
      if (category && description && title && tags && link && date && thumbnail) {
          await this.saveWebsiteSkeleton("single_skeleton",latestSkeleton)
          return true
      }
      if (title && tags && link && date && thumbnail) { 
          return true
      } 
      this.logger.logWarning(this.WebName,"validateSingleMainElements","One Of The Main Inputs Missing") 
      return false
    }

    async validateNavMainElements ($,latestSkeleton) {
      const dates = $("#dle-content article [itemprop='datePublished']").length > 0
      const links = $("#dle-content article .short_title a").length > 0
      if (dates && links) {
        await this.saveWebsiteSkeleton("navpage_skeleton",latestSkeleton)
        return true
      }  
      this.logger.logWarning(this.WebName,"validateNavMainElements","One Of The Main Inputs Missing")  
      return false 
    }
 

}

module.exports = Footfetishbb;