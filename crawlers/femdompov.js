const Crawler = require('./Crawler');
const cheerio = require('cheerio');
const request = require('request-promise');


class Femdompov extends Crawler {

    constructor () {
        super ("FemdomPOV",
            "https://femdom-pov.net/",
            "https://femdom-pov.net/page/",
            "https://femdom-pov.net/annabel-fatale-annabels-christmas-brain-wash-special-2019-mesmerise/")
        this.currentNavPage = 1
    }

    nextNavPage (page = null) {
        if(page){
            return this.WebNavUrl + page //Get A Specific Page Nav Url
        }
        return this.WebNavUrl + this.currentNavPage++
    }

    async crawlNavPage ( navUrl,videosTillDate = new Date(),videosFromDate = null) {
      //  console.log(navUrl)
      //  console.log("videosTillDate : " + videosTillDate + " videosFromDate : " + videosFromDate)
        let SingleUrls = []
        let varDate = null
        let SingleContent = null
        var options = {
            uri: navUrl,
            transform:   (body) => {
                const $ = cheerio.load(body);
                return $
            }
        };

        try {
            const $ =  await request(options)
            for ( let i = 0 ; i < $('.content_clr').children().length ; i++ )
            {
                //console.log('i : '+i)
                if($($('.content_clr').children()[i]).find('.main_box_tags').text().indexOf('MP3')>0 || $($('.content_clr').children()[i]).find('.main_box_tags').text().indexOf('Pictures')>0)
                    continue;
                SingleContent = await this.crawlSingle( $($('.content_clr').children()[i]).find('a.read_more').attr('href'))
                varDate =   SingleContent.date
                if( ( varDate <= videosTillDate) && (videosFromDate?( varDate >= videosFromDate):true) )
                    SingleUrls.push(SingleContent)

            }
            //console.log('fetched '+ SingleUrls.length)
            return SingleUrls
        }catch (e) {
            this.logger.logError(this.WebName+" crawlNavPage Request Failed "+e +" URL : "+url)
            return Promise.reject(this.WebName +' crawlNavPage Failed')
        }
    }

    async validateNavMainElements ($,latestSkeleton) {
        const links = $('.content_clr div').length > 0
        if (links) {
            await this.saveWebsiteSkeleton("navpage_skeleton",latestSkeleton)
            return true
        }
        this.logger.logWarning(this.WebName,"validateNavMainElements","One Of The Main Inputs Missing")
        return false
    }

    async crawlSingle (url = this.WebSingleUrl) {
       // console.log(url)
        let videoObject = {
            name: null,
            description: null,
            tags : null,
            category : null,
            thumbnail : null,
            link : null,
            source : this.WebName,
            date : null ,
        }
        var options = {
            uri: url,
            transform:   (body) => {
                const $ = cheerio.load(body);
                videoObject.name = $(".leftcolumn h1").text().trim();
                videoObject.description = $(".post_inner p[style*='center']").text().trim().substring($(".post_inner p[style*='center']").text().trim().indexOf("Keywords:"),-1)
                videoObject.tags = this.arrayToJson($(".post_inner p[style*='center']").text().trim().substring($(".post_inner p[style*='center']").text().trim().indexOf("Keywords:"),$(".post_inner p[style*='center']").text().trim().indexOf("mp4")).replace('Keywords: ','').split(','))
                videoObject.category = $('.tags_post a').first().text().trim()
                videoObject.thumbnail = $('.post_inner img').first().attr('src')
                videoObject.link = $('iframe').attr('src').replace('preview','file').replace('?site=femdom-pov.net','')
                videoObject.date = new Date($('.post_date').text())

                return videoObject
            }
        };
        try {
            return await request(options)
        }catch (e) {
            this.logger.logError(this.WebName+" crawlSingle Request Failed "+e +" URL : "+url)
            return Promise.reject(this.WebName +' crawlSingle Failed')
        }
    }

    async validateSingleMainElements ($,latestSkeleton) {
        const title = $(".leftcolumn h1").length == 1
        const description = $(".post_inner p[style*='center']").text().trim().indexOf("Keywords:")> 0
        const tags  = $(".post_inner p[style*='center']").text().trim().indexOf("mp4") > 0
        const category = $('.tags_post a').length > 0
        const thumbnail = $('.post_inner img').length > 0
        const link = $('iframe').length > 0
        const date = $('.post_date').length > 0

        if (!(category && description)) {
            this.logger.logWarning(this.WebName,"validateSingleMainElements","Category OR Description Missing")

        }
        if (category && description && title && tags && link && date && thumbnail) {
            await this.saveWebsiteSkeleton("single_skeleton",latestSkeleton)
            return true
        }
        if (title && tags && link && date && thumbnail) {
            return true
        }
        this.logger.logWarning(this.WebName,"validateSingleMainElements","One Of The Main Inputs Missing")
        return false
    }

    async getLatestVideos (options = null) {

        const webtest = await this.WebsiteTestSkeleton();

        if (!webtest){
            this.logger.logError(this.WebName + " GetLatest Videos WebsitestSkeleton Failed")
            return Promise.reject('WebSkeleton Verification Failed')
        }

        options = {
            numberOfVideos : options.numberOfVideos?options.numberOfVideos:10,
            videosTillDate : options.videosTillDate?new Date(options.videosTillDate):new Date(),
            videosFromDate : options.videosFromDate?new Date(options.videosFromDate):null,
        }
        this.currentNavPage = options.startPage?options.startPage:1


        let videosList = [];
        let Singlecontent = null;
        let singleUrls = await this.crawlNavPages(options)
        let isInLibrary = null
        let insertedCount = 0;


        for(let i = 0 ; i< singleUrls.length && i< options.numberOfVideos ; i++) {
            Singlecontent = singleUrls[i]
            isInLibrary = await this.checkVideoInLibrary(Singlecontent.name)
            if (!isInLibrary){
                await this.saveVideo(Singlecontent)
                insertedCount++;
            }

        }
        return Promise.resolve({ website : this.WebName, inserted : insertedCount  });
    }


}

module.exports = Femdompov;