const Video = require('../models/video.model');
 
exports.add_video = function (req, res,next) {
    let videoInstance = new Video(
        {
            name: req.body.name,
            body: req.body.body,
            url : req.body.url,
            last_crawl : req.body.date
        }
    );
    videoInstance.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Video Created successfully')
    })
};

exports.get_video = function (req, res,next) {
    Video.findById(req.params.id, function (err, video) {
        if (err) return next(err);
        res.send(video);
    })
};

exports.update_video = function (req, res,next) {
    Video.updateOne({ _id: req.params.id}, {$set: req.body}, function (err, video) {
        if (err) return next(err);
        res.send(video);
    });
};

exports.delete_video = function (req, res,next) {
    Video.deleteOne({ _id: req.params.id}, function (err) {
        if (err) return next(err);
        res.send('Video deleted successfully!');
    })
};