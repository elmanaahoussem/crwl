const request = require('request-promise')
const Logger = require('../controllers/Logger')
const videoModel = require('../models/video.model');


class FrontApi {

    static async submitVideo (video) {
        let ObjectTosend = video
        console.log(ObjectTosend)
        let logger = new Logger();
        let options = {
            method: 'POST',
            uri: process.env.FRONTAPIURL +"/videos/",
            formData: {
                name : ObjectTosend.name,
                description : ObjectTosend.description,
                thumbnail:ObjectTosend.thumbnail,
                category:ObjectTosend.category,
                tags:ObjectTosend.tags,
                gounlimited:ObjectTosend.gounlimited?ObjectTosend.gounlimited:"Unavailable",
                k2s:ObjectTosend.link?ObjectTosend.link:"Unavailable",
            },
            transform: async (resp) => {
                const rslt = JSON.parse(resp)
                await FrontApi.updateVideoField(ObjectTosend._id,"idfront",rslt.ID)
                await FrontApi.updateVideoField(ObjectTosend._id,"urlfront",rslt.guid)
                return resp
            }
        }

        try {
            return await request(options)
        }catch (e) {
            logger.logError("Submit Video For FrontApi failed " + e)
            return Promise.reject("Submit Video For FrontApi failed ")
        }
    }

   static async updateVideoField (videoId,field,value) {
        let data = {}
        data[field]  = value
        const result = await videoModel.updateOne (
            { _id: videoId }, data, {upsert : true})
            .catch (e =>{     this.logger.logError(field + ' -- Field  : updateVideoField Error : '   + e )     } )
        return result
    }

}

module.exports = FrontApi;