const debug = require('debug')('StreamingApi Module')
const request = require('request-promise')
const PromiseFtp = require('promise-ftp')
const fs = require('fs')
const Logger = require('../controllers/Logger')
const cheerio = require('cheerio');
const path = require("path");

class StreamingApi {

    constructor (host,key) {
        StreamingApi.ACCEPTED = "OK"
        this.logger = new Logger()
        this.host = host
        this.key = key
    }

    async TryConnection () {
        let url =  this.host +'api/account/info?key='+ this.key
        let options = {
            uri: url,
            transform: (body) => {
                let Response = JSON.parse(body)
                if (Response.msg == StreamingApi.ACCEPTED)
                return true
                return false
            }
        }
        try {
            return await request(options)
        }catch (e) {
            this.logger.logError( "TryConnection On StreamingApi : " + this.host + " Failed " + e)
            return Promise.reject("TryConnection On StreamingApi : " + this.host + " Failed " + e)
        }
    }

    async getServer() {
        let url = this.host + "api/upload/server?key=" + this.key
        var options = {
            uri: url,
            transform: (body) => {
                let resp =  JSON.parse(body)
                if (resp.msg == StreamingApi.ACCEPTED)
                return resp.result
                return false
            }
        }
        try {
            return await request(options)
        }catch (e) {
            this.logger.logError( "getServer On StreamingApi : " + this.host + " Failed :  URL " + url)
            return Promise.reject("getServer On StreamingApi : " + this.host + " Failed :  URL " + url)
        }
    }

    async uploadFile (fileUri) {

        if(!( await this.TryConnection()))
            return Promise.reject("Unable To Connect to : " + this.host + "uploadFile Failed" )

        let serverURL = await this.getServer()
        if (!serverURL)
            return Promise.reject("Pas De Serveur Valid")

        var options = {
            method: 'POST',
            uri: serverURL,
            formData: {
                api_key : this.key,
                file: {
                    value: fs.createReadStream(fileUri),
                    options: {
                        filename: path.basename(fileUri),
                        contentType: 'video/mp4'
                    }
                }
            },
            transform: (body) => {
               let $ =  cheerio.load(body);
               return this.host + $("textarea[name='fn']").text().trim()
            }
        }

        try {
            return await request(options)
        }catch (e) {
            this.logger.logError( "UploadFile On StreamingApi : " + this.host + " Failed ")
            return Promise.reject("UploadFile On StreamingApi : " + this.host + " Failed ")
        }

    }


    ftpUploadFile() {
        let ftp = new PromiseFtp();
        ftp.connect({host: "ftp.vidlox.me", user: "xcrawler", password: "a1z2e3r4t5"})
            .then(function (serverMessage) {
                console.log('connected')
                return ftp.put('1.mp4', '12.mp4');
            }).then(function (msg) {
                console.log('msg : ', msg)
            console.log('ended')
            return ftp.end();
        });
    }

}

module.exports = StreamingApi;